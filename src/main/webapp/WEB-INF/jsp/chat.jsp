<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Testing websockets</title>
</head>
<body>
<fieldset>
    <div style="float:left; width:390px !important; ">
        <textarea id="messageBox" rows="10" cols="50" readonly="true"></textarea>
        <br/>
        <input id="inputMessage" type="text"/>
        <input type="submit" value="send" onclick="send()" />
    </div>
    <div style="float:left; width:calc(100% - 390px);">
        <textarea id="userName" rows="10" cols="10" readonly="true"></textarea>
    </div>
</fieldset>
</body>
<script type="text/javascript">
    if ("WebSocket" in window)
    {
        var textarea = document.getElementById("messageBox");
        var userName = document.getElementById("userName");
        var inputMessage = document.getElementById('inputMessage');
        var ws = new WebSocket("ws://localhost:8080/websocket");

        ws.onopen = function(evt) {
            textarea.value += "연결 성공\n";
        };

        ws.onmessage = function(evt) {
            if(evt.data.indexOf("@@@@@@@") != -1) {
                userName.value += evt.data;
            } else {
                textarea.value += "상대 : " + evt.data + "\n";
            }
        };

        ws.onclose = function() {
            textarea.value += "연결 종료\n";
        };

        ws.onerror = function(evt) {
             alert(evt.data);
        }
    }

    function send() {
        textarea.value += "나 : " + inputMessage.value + "\n";
        ws.send(inputMessage.value);
        inputMessage.value = "";
    }
</script>
</html>