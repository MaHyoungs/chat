package com.chat;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@EnableAutoConfiguration
public class ClientController {

    @RequestMapping("/chat")
    public String ChatClient() {
        return "chat";
    }
}
