package com.chat;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@ServerEndpoint("/websocket")
public class ServerController {

    public static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());

    @OnOpen
    public void open(Session session) throws Exception {
        System.out.println(session);
        clients.add(session);

        synchronized (clients) {
            for (Session client : clients) {
                client.getAsyncRemote().sendText("@@@@@@@cleint");
            }
        }
    }

    @OnMessage
    public void message(String message, Session session) throws Exception {
        synchronized (clients) {
            for (Session client : clients) {
                if (!client.equals(session)) {
                    client.getBasicRemote().sendText(message);
                }
            }
        }
    }

    @OnClose
    public void close(Session session) {
        clients.remove(session);
        System.out.println("client disconnect");
    }

    @OnError
    public void error(Throwable e, Session session) {
        System.out.println("client force close");
    }
}
